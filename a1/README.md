# LIS4368 - Advanced Web Applications

## Bretania Forbes

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

### README.md includes the following items:
* Screenshot of running java Hello
* Screenshot of running http://localhost9999
* Git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial (bitbucketstationlocations).

### Git commands w/short descriptions:

1. git init - Creates a new local repository
2. git status - List the files you've changed and those you still need to add or commit 
3. git add - Add one or more files to staging (index)
4. git commit - Commit any files you've added with 'git add', and also commit any files you've 
changed since then
5. git push - Push all branches to your remote repository
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git branch - List all the branches in your repo, also tell you what branch you're currently 
in

### Assignment Screenshots:

*Screenshot of running Java Hello:*

![Hello World Screenshot](img/hello-world.PNG)

*Screenshot of running http://localhost:9999*

![Tomcat Server](img/tomcat-server.PNG)

### Assignment Links:
[Local LIS4368 Web App](http://localhost:9999/lis4368 "local web app")

[Assignment 1 Repo Link](https://bitbucket.org/forbesbkg/lis4368/src/master/a1/ "A1")