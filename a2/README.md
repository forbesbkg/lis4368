# LIS4368 - Advanced Web Applications

## Bretania Forbes

### Assignment 2 Requirements:

*Three Parts:*

1. MySQL Development Installation
2. Develop and Deploy Tomcat Webapp
3. Chapter Questions (Chs 5-6)

### README.md includes the following items:
* Screenshots of running http://localhost9999
* Bitbucket repo links: a) this assignment and b) the completed tutorial

### Assignment Screenshots:
(http://localhost:9999/hello)
![hello](img/directories.png)

(http://localhost:9999/hello/index.html)
![index](img/home.png)

(http://localhost:9999/hello/sayhello)
![sayhello](img/sayHello.png)

(http://localhost:9999/hello/querybook.html)
![hello-world](img/querybook.png)

(http://localhost:9999/hello/sayhi)
![Hello World Again](img/helloAgain.png)


![Results](img/queryresponse.png)


### Assignment Links:
[Local Hello Web App](http://localhost:9999/hello "local web app")

[Assignment 2 Repo Link](https://bitbucket.org/forbesbkg/lis4368/src/master/a2/ "A1")