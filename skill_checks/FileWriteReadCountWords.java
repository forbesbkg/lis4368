import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;

public class FileWriteReadCountWords{
    public static void main(String [] args) throws IOException{

        String text, file = "filecountwords.txt";
        int count = 0;

        Scanner read = new Scanner(System.in);
        PrintWriter out = new PrintWriter(file);

        System.out.print("\nPlease enter text: ");
        text = read.nextLine();

        out.println(text);

        System.out.println("Saved to file \"filecountwords.txt\"");

        Scanner in = new Scanner(text);

        while(in.hasNext()){
            count++;
            in.next();
        }

        System.out.println("Number of words: " + count + '\n');

        read.close();
        in.close();
        out.close();

    }
}