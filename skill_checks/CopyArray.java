import java.util.Scanner;

public class CopyArray{

    public static void main(String []args){

        String [] str1 = {"Apples", "Banana", "Carrot", "Dill", "Elderberry"};
        String [] str2 = new String[str1.length];


        for (int i = 0; i < str2.length; i++){
            str2[i] = str1[i];
        }


        System.out.println("\nProgram Requirements:");
        System.out.println("1. Create a five element string array (str1).");
        System.out.println("2. Create a second string array (str2) based upon the length of the str1 array.");
        System.out.println("3. Copy str1 array elements into str2.");
        System.out.println("4. Print elements of both arrays using Java's *enhanced* for loop.");


        System.out.println("\nPrinting str1 array: ");
        for (String element : str1){
            System.out.println(element);
        }

        System.out.println("\nPrinting str2 array: ");
        for (String element : str2){
            System.out.println(element);
        }
        
        
    }

}