import java.util.Scanner;

public class CountCharacters{
    public static void main (String [] args){
        String phrase;
        char[] charArray;
        int letterCount = 0, spaceCount = 0, numCount = 0, otherCount = 0;

        Scanner read = new Scanner(System.in);

        System.out.print("Please enter string: ");

        phrase = read.nextLine();

        charArray = phrase.toCharArray();

        for (char character: charArray){        
            if (Character.isLetter(character)){
                letterCount++;
            }
            else if (Character.isDigit(character)){
                numCount++;
            }
            else if (Character.isSpaceChar(character)){
                spaceCount++;
            }
            else{
                otherCount++;
            }
        }



        System.out.println("\nYour string: \"" + phrase + "\" has the following number and types of characters:");
        System.out.println("letter(s): " + letterCount);
        System.out.println("space(s): " + spaceCount);
        System.out.println("number(s): " + numCount);
        System.out.println("other character(s): " + otherCount);






    }
}