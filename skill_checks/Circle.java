import java.util.Scanner;

public class Circle{

    public static void main(String []args){

        Scanner read = new Scanner(System.in);

        System.out.println("Enter circle radius: ");

        int radius = read.nextInt();

        if (!(new Integer(radius).isInt()))
        {
            System.out.println("Not valid number!");
            System.out.println("/n/nPlease try again. Enter circle radius: ");
        }

        System.out.println("Circle diameter: " + (radius * 2));
        System.out.println("/nCircumfrence: " + (radius * 2 * 3.14));
        System.out.println("/nArea: " + (radius * radius * 3.14));



    }
}