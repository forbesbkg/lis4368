import java.util.Scanner;

public class NumberSwap{

    public static void main (String [] args){

        Scanner read = new Scanner(System.in);
        int num1, num2; 

        System.out.println("Program swaps two integers.");
        System.out.println("Note: Program checks for integers and non-numeric values.\n");


        System.out.print("Please enter first number: ");

        while(!read.hasNextInt()){
            System.out.println("Not valid integer!\n");
            read.next();
            System.out.print("Please try again. Enter first number: ");
        }

        num1 = read.nextInt();

        System.out.print("\nPlease enter second number: ");

        while(!read.hasNextInt()){
            System.out.println("Not valid integer!\n");
            read.next();
            System.out.print("Please try again. Enter second number: ");
        }

        num2 = read.nextInt();

        System.out.println("\nBefore swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);

        int temp = num1;
        num1 = num2;
        num2 = temp;


        System.out.println("\nAfter swapping");
        System.out.println("num1 = " + num1);
        System.out.println("num2 = " + num2);

        read.close();






    }
}