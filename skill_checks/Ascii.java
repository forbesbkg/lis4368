import java.util.Scanner;

public class Ascii{

    public static void main (String [] args){

        Scanner read = new Scanner(System.in);
        boolean isValidNum = false;
        char alpha;
        int num;
    
        System.out.println("\nPrinting characters A-Z as ASCII values:");

        for (alpha = 'A'; alpha <= 'Z'; alpha++){
            System.out.println("Character " + alpha + " has ascii value " + (int)alpha);
        }

        System.out.println("\nPrinting ASCII values 48-122 as characters:");

        for (num = 48; num <=122; num++){
            System.out.println("ASCII value " + num + " has character value "+ (char)num);
        }

        System.out.println("\nAllowing user ASCII value input:");

        while (isValidNum == false){
            System.out.print("Please enter ASCII value (32 - 127): ");
            if (read.hasNextInt()){
                num = read.nextInt();
                isValidNum = true;
            }
            else{
                System.out.println("Invalid integer--ASCII value must be a number.\n");
            }

            read.nextLine();

            if (isValidNum == true && (num < 32 || num > 127)){
                System.out.println("ASCII value must be >= 32 and <= 127\n");
                isValidNum = false;
            }

            if (isValidNum == true){
                System.out.println();
            }


            
        }

        System.out.println("\nASCII value "+ num + " has character value "+ (char)num + '\n');

        read.close();
    }
    
}