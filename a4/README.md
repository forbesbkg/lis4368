# LIS4368 - Advanced Web Applications

## Bretania Forbes

### Assignment 4 Requirements:


### README.md includes the following items:
* Screenshots of running http://localhost9999
* Bitbucket repo links: a) this assignment and b) the completed tutorial

### Assignment Screenshots:
(Valid Input)
![valid](img/1.png)

(Invalid Input)
![invalid](img/2.png)



### Assignment Links:
[Local Hello Web App](http://localhost:9999/hello "local web app")

[Assignment 4 Repo Link](https://bitbucket.org/forbesbkg/lis4368/src/master/a4/ "A4")