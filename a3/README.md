# LIS4368 - Advanced Web Applications

## Bretania Forbes

### Assignment 3 Requirements:

*Deliverables*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records each table)
3. Provide Bitbucket read-only access to repo (Language SQL), *must* include README.md using markdown syntax, and include links to *all* of the following files 
    * docs folder: a3.mwb and a3.sql
    * img folder: a3.png
    * README.md
4. Links: Assignment repo

### README.md includes the following items:
* Screenshots of running A3 ERD
* Links to files
* Bitbucket repo link

### Assignment Screenshot and Links:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")

[Assignment 3 Repo Link](https://bitbucket.org/forbesbkg/lis4368/src/master/a3/ "A3")


