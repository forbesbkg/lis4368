# LIS4368 - Advanced Web Applications

## Bretania Forbes

### Project 1 Requirements:

*Deliverables*

1. Use min/max JQuery validation
2. Use regexp to only allow appropriate characters for each control: 
    * cus_fname, cus_lname, cus_street, cus_city, cus_state, cus_zip, cus_phone, cus_email, cus_balance, cus_total_sales, cus_notes
    * fname, lname: provided
    * street, city:
        * no more than 30 characters
    * Street: must only contain letters, numbers, commas, hyphens, or periods
    * City: can only contain letters, numbers, hyphens, and space character (29 Palms)
    * state:
        * must be 2 characters
        * must only contain letters
    * zip:
        * must be between 5 and 9 characters, inclusive
        * must only contain numbers
    * phone:
        * must be 10 characters, including area code
        * must only contain numbers
    * email: provided
    * balance, total_sales:
        * no more than 6 digits, including decimal point can only contain numbers, and decimal point (if used)

### README.md includes the following items:
* Screenshots of running validation
* Links to web-server

### Assignment Screenshot and Links:

![P1 Validation](img/error.png "Client-Side Validation w/ Errors")

![P1 Validation2](img/correct-values.png "Client-Side Validation w/ Correct Values")



[P1 Client-Side Validation Link](http://localhost:9999/lis4368/p1/index.jsp "P1")


