# LIS4368 - Advanced Web Applications

## Bretania Forbes

### Project 2 Requirements:


### README.md includes the following items:
* Screenshots of running http://localhost9999
* Bitbucket repo links

### Assignment Screenshots:

|![Valid User Form Entry](img/1.png) | ![Passed validation](img/2.png)
|![Display Data](img/3.png) | ![Passed validation](img/4.png)
|![Valid User Form Entry](img/5.png) | ![Passed validation](img/6.png)




### Assignment Links:
[Local LIS4368 Web App](http://localhost:9999/hello "local web app")

[Project 2 Repo Link](https://bitbucket.org/forbesbkg/lis4368/src/master/p2/ "P2")