# LIS4331 - Advanced Web Application Development

## Bretania Forbes

### LIS4368 Requirements:

1. [A1 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/a1/README.md "A1 Repo")
    * Install JDK
    * Install Tomcat
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket repo
    * Complete Bitbucket tutorials
    * Provide git command descriptions

2. [A2 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/a2/README.md "A2 Repo")
    * Install and Run MySQL
    * Develop and Deploy Tomcat App
    * Provide screenshots of development

3. [A3 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/a3/README.md "A3 Repo")
    * Entity Relationship Diagram (ERD)
    * Include data (at least 10 records each table)
    * Provide links to database files and sql statements

4. [P1 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/p1/README.md "P1 Repo")
    * Basic Client-Side Validation with JQuery
    * Use regexp to only allow appropriate characters for each control

5. [A4 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/a4/README.md "A4 Repo")
    * Basic Server-Side Validation
    * Use regexp to only allow appropriate characters for each control

6. [A5 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/a5/README.md "A5 Repo")
    * Review subdirectories
    * Use the MVC framework to write programs to inject SQL statements into database from validation form

6. [P2 README.md](https://bitbucket.org/forbesbkg/lis4368/src/master/p2/README.md "P2 Repo")
    * MVC Framework, using basic client and server side validation
    * Prepared statements to help prevent SQL injection
    * JSTL to prevent XSS
    * Completes CRUD functionality