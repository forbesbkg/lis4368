<%@page contentType="text/html" pageEncoding="utf-8"%>

<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Bretania Forbes, IT Student.">
	<link rel="icon" href="favicon.ico">

	<title>CRSXXXX - JSP Forms</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body>

	<%@ include file="/global/nav_global.jsp" %>	

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<%@ include file="/global/header.jsp" %>
					</div>

    <h3>Thanks for joining our customer list!</h3>

    <p>Here is the information that you entered:</p>

		<div class="col-xs-12 col-sm-offset-4 text-left">
			<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

			<% //<c:out... prevents cross-site scripting (XSS) attack (escapes output) %>
			<label>First Name:</label> <c:out value="${user.fname}" /> <br />
			<label>Last Name:</label> <c:out value="${user.lname}" /> <br />
			<Label>Street:</Label> <c:out value="${user.street}" /><br />
			<Label>City:</Label> <c:out value="${user.city}" /><br />
			<Label>State:</Label> <c:out value="${user.state}" /><br />
			<Label>Zip:</Label> <c:out value="${user.zip}" /><br />
			<Label>Phone:</Label> <c:out value="${user.phone}" /><br />
			<label>Email:</label> <c:out value="${user.email}" /> <br />
			<Label>Balance:</Label> <c:out value="${user.balance}" /><br />
			<Label>Total Sales:</Label> <c:out value="${user.totalSales}" /><br />
			<Label>Notes:</Label> <c:out value="${user.notes}" /><br />
			

			<p>To enter another record click Return.</p>

			<form method="post" class="form-horizontal" action="${pageContext.request.contextPath}/customerAdmin">
				<input type="hidden" name="action" value="join">
				<input type="submit" value="Return">
			</form>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->
		
</body>
</html>
